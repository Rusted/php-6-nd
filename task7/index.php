<?php
require_once 'adverts.php';

$totalAdverts = count($adverts);
$totalPaidAdverts = 0;
$totalPaidAdvertsSum = 0;
$totalUnpaidAdvertsSum = 0;
foreach ($adverts as $advert) {
    if ($advert['onPay']) {
        $totalPaidAdverts++;
        $totalPaidAdvertsSum += $advert['cost'];
    } else {
        $totalUnpaidAdvertsSum += $advert['cost'];
    }
}
?>

<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
        crossorigin="anonym ous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp"
        crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>
</head>
<body class="container">
<h1>Skelbimai</h1>
    <div class="well">
        Viso skelbimų: <?php echo $totalAdverts; ?><br>
        Viso apmokėtų skelbimų: <?php echo $totalPaidAdverts; ?><br>
        Apmokėtų skelbimų suma: <?php echo $totalPaidAdvertsSum; ?><br>
        Neapmokėtų skelbimų suma: <?php echo $totalUnpaidAdvertsSum; ?><br>
    </div>
    <table class="table">
        <tr>
            <th>Id</th>
            <th>Tekstas</th>
            <th>Kaina</th>
            <th>Paid on</th>
        </tr>
        <?php foreach ($adverts as $advert) {?>
        <tr>
            <th><?php echo $advert['id']; ?></th>
            <th><?php echo $advert['text']; ?></th>
            <th><?php echo $advert['cost']; ?></th>
            <th><?php echo $advert['onPay'] ? date('Y-m-d', $advert['onPay']) : ''; ?></th>
        </tr>
        <?php }?>
    </table>
</body>

</html>